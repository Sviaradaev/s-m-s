<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Активные заказы</title>
	</head>
	<body>
    <?php include 'templates/DbConnection.php';
    $query = "SELECT * FROM orders";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for ($data = []; $row = mysqli_fetch_assoc($result); $data[] = $row);

    $separator = '----';
	$result = '';
	
	foreach ($data as $elem) {
		$result .= '<p>';
		
		$result .= '<b>' . $elem['name'] . $separator . $elem['date'] . $separator . $elem['status'] . '</b>';
		
		$result .= '</p>';
	}
	
	echo $result; ?>
	</body>
</html>